<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Permission;
use App\Models\User;
use App\Models\EstadosUsuario;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $estados1 = EstadosUsuario::create([
            'name' => 'Activo'
        ]);
        $estados2 = EstadosUsuario::create([
            'name' => 'Inactivo'
        ]);
        $rol = Role::create(['name' => 'Administrador']);

        $permiso = Permission::create(['name' => 'index_permissions']);
        $rol->givePermissionTo($permiso->name);
        $user = User::create([
            'name' => 'Administrador',
            'email' => 'Administrador@konecta.com',
            'password' => '$2y$10$BstW4e3tu4v9qCt4AXORLebYOP8FqZe1wzow11fAnGozTiYevShJa', // secret
            'remember_token' => null,
            'role_id' => $rol->id,  
            'estado_id' => 1,
        ]);
        $user->assignRole($rol->name);
        app()['cache']->forget('spatie.permission.cache');
    }
}
