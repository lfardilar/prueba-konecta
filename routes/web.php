<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Http\Controllers\PermisionsController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UsuariosController;
use App\Http\Controllers\MarcaMedidorController;
use App\Http\Controllers\ModeloMedidorController;
use App\Http\Controllers\ClienteController;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Auth/Login', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');
Route::post('login', 'App\Http\Controllers\AuthController@authenticate');

//Administracion
//Usuarios
Route::group(['middleware' => ['permission:index_users']], function () {
    Route::middleware(['auth:sanctum', 'verified'])->get('/usuarios', function () {
        return Inertia::render('Administracion/Usuarios/index');
    })->name('usuarios.index');
});
Route::group(['middleware' => ['permission:acciones_usuarios','jwt.verify']], function () {
    Route::post('getUsuarios', [UsuariosController::class, 'getUsuarios'])->name('usuarios.getUsuarios');
    Route::post('getSelectRoles', [UsuariosController::class, 'getSelectRoles'])->name('usuarios.getSelectRoles');
    Route::post('crearUsuario', [UsuariosController::class, 'store'])->name('usuarios.crearUsuario'); 
    Route::post('editarUsuario', [UsuariosController::class, 'update'])->name('usuarios.editarUsuario'); 
});
//Roles
Route::group(['middleware' => ['permission:index_roles']], function () {
    Route::middleware(['auth:sanctum', 'verified'])->get('/roles', function () {
        return Inertia::render('Administracion/Roles/index');
    })->name('roles.index');
    Route::middleware(['auth:sanctum', 'verified'])->post('/asignarRolUsuarios', function (Request $request) {       
        return Inertia::render('Administracion/Roles/AsignarRolUsuarios', ['rolAsig' => $request]);
    })->name('roles.asignarRolUsuarios'); 
    Route::middleware(['auth:sanctum', 'verified'])->post('/asignarPermisosRol', function (Request $request) {       
        return Inertia::render('Administracion/Roles/AsignarPermisosRol', ['rolAsig' => $request]);
    })->name('roles.asignarRolUsuarios'); 
});
Route::group(['middleware' => ['permission:acciones_roles', 'jwt.verify']], function () {
    Route::post('getRoles', [PermisionsController::class, 'getRoles'])->name('roles.getRoles');
    Route::post('crearRole', [PermisionsController::class, 'store'])->name('roles.crearRole'); 
    Route::post('editarRole', [PermisionsController::class, 'update'])->name('roles.editarRole'); 
    Route::post('eliminarRole', [PermisionsController::class, 'deleteRol'])->name('roles.eliminarRole'); 
    Route::post('obtenerUsuariosAsignadosRol', [PermisionsController::class, 'ajaxPrintUsersRol'])->name('permissions.ajaxPrintUsersRol');
    Route::post('obtenerPermisosAsignadoRol', [PermisionsController::class, 'ajaxPrintPermissionsRol'])->name('permissions.ajaxPrintPermissionsRol');
    Route::post('agregarUsuarioRol', [PermisionsController::class, 'ajaxAdminUsersRol'])->name('permissions.ajaxAdminUsersRol');
    Route::post('agregarPermisoRol', [PermisionsController::class, 'ajaxAdminPermissionsRol'])->name('permissions.ajaxAdminPermissionsRol');
});
//Clientes
Route::group(['middleware' => ['permission:index_clientes']], function () {
    Route::middleware(['auth:sanctum', 'verified'])->get('/clientes', function () {
        return Inertia::render('Administracion/Clientes/index');
    })->name('clientes.index');   
});
Route::group(['middleware' => ['permission:acciones_clientes', 'jwt.verify']], function () {
    Route::post('getClientes', [ClienteController::class, 'getClientes'])->name('clientes.getClientes');
    Route::post('crearCliente', [ClienteController::class, 'store'])->name('clientes.crearCliente');
    Route::post('editarCliente', [ClienteController::class, 'update'])->name('clientes.editarCliente');
    Route::post('eliminarCliente', [ClienteController::class, 'destroy'])->name('clientes.eliminarCliente');       
    Route::post('getSelect', [ClienteController::class, 'getSelect'])->name('clientes.getSelect');       
});