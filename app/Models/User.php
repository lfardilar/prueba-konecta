<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
        'estado_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];
    
    public  function _validatePermission($permission = ''){

        if(!self::_isAdmin()){
            $permission = explode(',',$permission);

            foreach ( $permission as $key =>  $value) {
                $permission[$key] = trim($value);
            }

            $validate = DB::table('role_has_permissions AS rp')
                ->select(DB::raw('count(rp.permission_id) AS cant'))
                ->join('permissions  AS p','rp.permission_id','=','p.id')
                //->where(['p.name' => "$permission"])
                ->whereIn('p.name',$permission)
                ->whereIn('rp.role_id', function ($query){
                    $query->select(DB::raw('mr.role_id'))
                        ->from('model_has_roles AS mr')
                        ->where(['mr.model_id' =>  Auth::user()->id ]);
                })
                ->first();
            return (intval($validate->cant)==0)?false:true;
        } else {
            return true;
        }
    }

    public static function _isAdmin(){

        $data = DB::table('model_has_roles  AS mr')
                    ->leftJoin('users AS u', 'u.id', '=','mr.model_id')
                    ->where([
                            'mr.model_id' =>Auth::user()->id,
                            'mr.role_id' => 1
                        ])
                    ->count();
        return ($data>0)?true:false;
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
