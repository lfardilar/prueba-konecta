<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Http\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Http\Exceptions\TokenExpiredException;

class JwtMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) 
    {
        try {           
            $user = JWTAuth::parseToken()->authenticate();            
        } catch (Exception $e) {            
            if ($e instanceof TokenInvalidException){
                return response()->json(['message' => 'Token Invalido'], 400);
            }else if ($e instanceof TokenExpiredException){
                return response()->json(['message' => 'El Token de autorización a Caducado'], 400);
            }else{
                return response()->json(['message' => 'Token de autorización no encontrado'], 400);
            }
        }
        return $next($request);
    }
}
