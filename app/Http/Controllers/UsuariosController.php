<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Actions\Fortify\PasswordValidationRules;

class UsuariosController extends Controller
{
    
    use PasswordValidationRules;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Consultar los usuarios registrados
    */
    public function getUsuarios(Request $request){
        $users = User::select('email', 'users.name AS usuario', 'users.id', 'estado_id', 'role_id')
        ->join('roles as rol', 'rol.id','=','users.role_id')
        ->get();
        foreach ($users as $user) {
            foreach ($user->getRoleNames() as $rol) {
                $perfil = $rol;
                $user['perfil'] = $perfil;
            }
        }
        return response()->json($users);
    }
    /**
     * Consultar los roles para formulario crear/editar usuarios
    */
    public function getSelectRoles(Request $request){
        $roles = Role::select('name','id')->get();       
        return response()->json($roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            //valida las condiciones del registro
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => $this->passwordRules(),
                'role_id' => ['required', 'int']
            ]);
            if ($validator->fails()) {
                throw new \Exception($validator->messages());
            }
            //Inicia el registro
            $role_r =  Role::findById($request['role_id']);
            $user = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'role_id' => $request->input('role_id'),
                'estado_id' => 1,
            ]);
            $user->assignRole($role_r); //Assigning role to user
            // evento de nuevo registro
        
            $users = User::select('email', 'users.name AS usuario', 'users.id', 'estado_id', 'role_id')
            ->join('roles as rol', 'rol.id','=','users.role_id')
            ->where('users.id', $user->id)
            ->first();
            foreach ($users->getRoleNames() as $rol) {
                $perfil = $rol;
                $users['perfil'] = $perfil;
            }
            return response()->json(['message' => "Usuario Creado Exitosamente", "data" => $users], 200);
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            //valida las condiciones del registro
            $validarEmail = User::where('email',$request->email)->where('id',"!=",$request->id)->count();
            if ($validarEmail>0) {
                throw new \Exception("Email ".$request->email." Ya se encuentra registrado");
            } 
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'role_id' => ['required', 'int'],
                'estado_id' => ['required', 'int']
            ]);
            if ($validator->fails()) {
                throw new \Exception($validator->messages());
            }           
            $usuario = User::find($request->id);
            $role_old =  Role::findById($usuario->role_id);
            $role_r =  Role::findById($request->role_id);
            $user = User::find($request->id)->update([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'role_id' => $request->input('role_id'),
                'estado_id' => $request->input('estado_id'),
            ]);
            $usuario->removeRole($role_old);
            $usuario->assignRole($role_r); //Assigning role to user
        
            $users = User::select('email', 'users.name AS usuario', 'users.id', 'estado_id', 'role_id')
            ->join('roles as rol', 'rol.id','=','users.role_id')
            ->where('users.id', $request->id)
            ->first();
            foreach ($users->getRoleNames() as $rol) {
                $perfil = $rol;
                $users['perfil'] = $perfil;
            }
            return response()->json(['message' => "Usuario Editado Exitosamente", "data" => $users], 200);
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()." - ".$e->getLine()], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
