<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Validator;

class PermisionsController extends Controller
{
    public $permissionsList;
    public $detailListPermissions;
    public $asigSuperAdmin;
    public function __construct()
    {
        $this->detailListPermissions = [];
        $this->permissionsList = [];
        $this->asigSuperAdmin = false;
    }


    public function index(){
        $this->_detailListPermissions(true);
        return view('permisions.index',[
            'routes' => $this->detailListPermissions
        ]);
    }


    public function rolsList(){
        return view('permisions.rolsList');
    }
    /**
     * Consultar los roles 
     */
    public function getRoles(Request $request)
    {
        $roles = Role::select('id', 'name AS role', 'guard_name', 'created_at', 'updated_at')->get();
        return response()->json($roles);
    }
    
    public function createRol(){
        return view('permisions.createRol');
    }

    /**
     * Funcion para crear role
     */
    public function store(Request $request)
    {
        try{
            $valida = Role::where('name',$request->role)->count();
            if($valida > 0){
                $rol = $request->role;
                throw new \Exception("El rol " .$rol. " Ya existe");
            }
            $role = new Role();
            $role->fill(['name' =>$request->role]);
            $validator = Validator::make($role->attributesToArray(),[
                'name' => 'required'
            ]);
            if ($validator->fails())
                throw new \Exception($validator->messages());

            $role->save();
            $roles = Role::select('id', 'name AS role', 'guard_name', 'created_at', 'updated_at')->where('id',$role->id)->first();
            return response()->json(['message' => "Role Creado Exitosamente", "data" => $roles], 200);
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 404);
        }
    }
    /**
     * Funcion para editar role
     */
    public function update(Request $request)
    {
        try{
            $valida = Role::where('name',$request->role)->where('id','!=',$request->id)->count();
            if($valida > 0){
                $rol = $request->role;
                throw new \Exception("El rol " .$rol. " Ya existe");
            }
            $validator = Validator::make($request->all(), [
                'role' => ['required', 'string', 'max:255'],               
            ]);
            if ($validator->fails()) {
                throw new \Exception($validator->messages());
            }
            $role = Role::where('id', $request->id)->update(["name"=>$request->role]);
            $roles = Role::select('id', 'name AS role', 'guard_name', 'created_at', 'updated_at')->where('id',$request->id)->first();
            return response()->json(['message' => "Role Editado Exitosamente", "data" => $roles], 200);
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()." - ".$e->getLine()], 404);
        }
    }

    public function permissionsRol(Role $role){
        return $role;
    }
    /**
     * Consulta los permisos asignados al rol
    */
    public function ajaxPrintPermissionsRol(Request $request){
        $permissions = DB::table('role_has_permissions AS rp')
            ->select(['p.name AS permission','p.id'])
            ->leftJoin('permissions AS p','p.id','=','rp.permission_id')
            ->where(['rp.role_id' => $request->id])
            ->get()
            ->toArray();
        $this->_detailListPermissions();
   
        return response()->json([
            'permissions' => $permissions,
            'routes' => $this->detailListPermissions
        ]);
    }
    /**
     * Agregar o quitar los permisos relacionados a rol 
    */
    public function ajaxAdminPermissionsRol(Request $request){
        try{
            $response = false;
            $message = "";
            $role = $request->role;
            if($request->input('stage')=='routes'){
                if($this->_hasPermissionRol($role['id'],$request->input('permission'))){
                    $response = false;
                    $message = 'El permiso '.$request->input('permission').' ya se encuentra asignado al rol '.$role['role'];
                    throw new \Exception($message);
                } else {
                    if($this->_givePermissionRol($role['id'],$request->input('permission'))){
                        $response = true;
                        $permissions = DB::table('role_has_permissions AS rp')
                        ->select(['p.name AS permiso','p.id'])
                        ->leftJoin('permissions AS p','p.id','=','rp.permission_id')
                        ->where(['rp.role_id' => $role['id']])
                        ->where('p.name', $request->input('permission'))
                        ->get();
                        return response()->json(['message' => "Permiso Asignado Del Rol Exitosamente", "data" => $permissions], 200);
                    } else {
                        $response = false;
                        $message='No ha sido posible agregar el permiso '.$request->input('permission').' al rol '.$role['role'];
                        throw new \Exception($message);
                    }
                }
            } elseif ($request->input('stage')=='permissions'){
                $role = Role::find($role['id']);
                $role->revokePermissionTo($request->input('permission'));
                return response()->json(['message' => "Permiso Removido Del Rol Exitosamente", "data" => $request->input('permission')], 200);
            }
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 404);
        }
    }

    /**
     * Consultar el rol 
    */
    public function usersRol(Request $request, $id)
    {
        $rol = Role::find($id);
        return $rol;
    }

    /**
     * Consultar los usuarios asignados al rol
    */
    public function ajaxPrintUsersRol(Request $request){
        $users = DB::table('users AS u')
            ->select(DB::raw("u.id, u.name AS usuario, u.email , (CASE (SELECT COUNT(*) FROM model_has_roles AS mr WHERE mr.model_id = u.id  AND mr.role_id = ".$request->id.") WHEN 1 THEN 'true' ELSE 'false' END) AS isadd"))
            ->where(DB::raw("(SELECT COUNT(*) FROM model_has_roles AS mr WHERE mr.model_id = u.id  AND mr.role_id = $request->id)"), 0)
            ->get()
            ->toArray();
        $usersHasRole = DB::table('model_has_roles AS mr')
            ->select(DB::raw("u.id, u.name AS usuario, u.email, (CASE (SELECT COUNT(*) FROM model_has_roles AS mr WHERE mr.model_id = u.id  AND mr.role_id = ".$request->id.") WHEN 1 THEN 'true' ELSE 'false' END) AS isadd"))
            ->join('users AS u','u.id','=', 'mr.model_id')
            ->where(['mr.role_id' => $request->id])            
            ->get()
            ->toArray();
        return response()->json([
            'users' => $users,
            'usersHasRole' => $usersHasRole
        ]);
    }
    /**
     * Agregar o quitar los usuarios relacionados a rol 
    */
    public function ajaxAdminUsersRol(Request $request){
        try{
            $role = $request->role;
            $response = false;
            $message = "";
            if(intval($request->input('idUser'))!=0){
                $user = User::where(['id' => $request->input('idUser')])->first();
                if($user!=null){
                    if($request->input('stage')=='add'){
                        if($user->hasRole($role['role'])){
                            $message = 'El usuario '.$user->name.' ya se encuentra asignado al rol '.$role['role'];
                            throw new \Exception($message);
                        } else {
                            if($this->_assignRole($user,$role['role'])){
                                $users = DB::table('model_has_roles AS mr')
                                ->select(DB::raw('u.id, u.name AS usuario, u.email '))
                                ->join('users AS u','u.id','=', 'mr.model_id')
                                ->where(['mr.role_id' => $role['id'], 'u.id' => $user->id])
                                ->get()
                                ->toArray();
                                return response()->json(['message' => "Usuario Agregado Al Rol Exitosamente", "data" => $users], 200);
                            } else {
                                $message='No ha sido posible agregar el usuario '.$user->name.' al rol '.$role['role'];
                                throw new \Exception($message);
                            }
                        }
                    } elseif ( $request->input('stage')=='remove' && intval($request->input('idUser'))!=0 ){
                        $user->removeRole($role['role']);
                        return response()->json(['message' => "Usuario Eliminado Del Rol Exitosamente", "data" => $user], 200);
                    }
                } else {
                    $message = 'No se ha logrado eliminar la asignacion del rol '.$role['role'].' al usuario';
                    throw new \Exception($message);
                }
            }else{
                $message = 'Datos incorrectos';
                throw new \Exception($message);
            }
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 404);
        }
       
    }

    /**
     * Eliminar el rol 
    */
    public function deleteRol(Request $request){
        try{
            $users = DB::table('model_has_roles')
                    ->where(['role_id' => $request->id])
                    ->count();
            $message = "";
            if($users>0){
                $message = "No es posible eliminar, EL rol ".$request->role." tiene $users usuarios asignados";
                throw new \Exception($message);
            }else{                
                $role = Role::find($request->id);
                $permissions = DB::table('role_has_permissions AS rp')
                                ->select('p.name')
                                ->join('permissions AS p','p.id','=','rp.permission_id')
                                ->where(['rp.role_id' => $request->id])
                                ->get()
                                ->toArray();
                foreach ($permissions as $permission){
                    $role->revokePermissionTo($permission->role);
                }
                Role::destroy($role->id);
            }
            return response()->json(['message' => "Rol Eliminado Exitosamente", "data" => $role], 200);  
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 404);
        }
    }

    /**
     * Generar la lista de permisos asociados al rol 
    */
    public function _detailListPermissions($asigSuperAdmin=false){
        $this->asigSuperAdmin = $asigSuperAdmin;
        collect(\Route::getRoutes())->map(function ($route) {
            if(sizeof($route->Middleware())>1){
                $permission = explode(":",$route->Middleware()[1]);
                if($permission[0]=="permission"){
                    if(!in_array($permission[1],$this->permissionsList)){
                        $this->_validatePermission($permission[1]);
                        array_push($this->permissionsList,$permission[1]);
                        $this->detailListPermissions[$permission[1]] = [
                            'permission' => $permission[1],
                            'methods' => implode('|',$route->methods)."-",
                            'urls' => $route->uri()."-"
                        ];
                    } else {
                        $this->detailListPermissions[$permission[1]]['urls'].=$route->uri()."-";
                        $this->detailListPermissions[$permission[1]]['methods'].=implode('|',$route->methods)."-";
                    }
                }
            }
        });
    }

    /**
     * Validar si el permiso esta creado
    */
    public function _validatePermission($name){
        $permission = Permission::where(['name' => $name])->first();
        if($permission==null)
            $permission =Permission::create(['name' => $name]);

        if($this->asigSuperAdmin){
            if(!$this->_hasPermissionRol(1,$permission->name))
                $this->_givePermissionRol(1,$permission->name);
        }
    }
    /**
     * Asigna el rol al usuario
    */
    public function _assignRole($user, $nameRole){
        $user->assignRole($nameRole);
        return $user->hasRole($nameRole);
    }

    
    public function _givePermissionRol($idRol, $namePermisison){
        $role = Role::where(['id' => $idRol])->first();
        $role->givePermissionTo($namePermisison);
        return $this->_hasPermissionRol($idRol, $namePermisison);
    }

    public function _hasPermissionRol($idRol, $namePermission){
        $role = Role::where(['id' => $idRol])->first();
        return $role->hasPermissionTo($namePermission);
    }
}
