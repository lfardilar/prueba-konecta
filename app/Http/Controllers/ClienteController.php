<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;
use Illuminate\Support\Facades\Validator;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Consultar los clientes registrados
     */
    public function getClientes(Request $request)
    {
        $clientes = Cliente::orderBy('nombre', 'asc')->get();
        return response()->json($clientes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{ 
            //Valida las condiciones del registro
            $validator = Validator::make($request->all(), [
                'nombre' => ['required', 'string'],
                'nit' => ['required', 'string', 'min:4']
            ]);
            if ($validator->fails()) {
                throw new \Exception($validator->messages());
            }
            $cont = Cliente::where('documento',$request->input('nit'))->count();
            if ($cont == 0) {
                $cliente = Cliente::create([
                    'nombre' => $request->input('nombre'),
                    'documento' => $request->input('nit'),
                    'correo' => $request->input('email'),              
                    'direccion' => $request->input('direccion')                  
                ]);            
                return response()->json(['message' => "Cliente Creado Exitosamente", "data" => $cliente], 200); 
            } else {
                throw new \Exception("El cliente con nit o documento ".$request->nit." ya existe");   
            }
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{            
            //Valida las condiciones del registro
            $validator = Validator::make($request->all(), [
                'nombre' => ['required', 'string'],
                'nit' => ['required', 'string', 'min:4']
            ]);
            if ($validator->fails()) {
                throw new \Exception($validator->messages());
            }
            
            $cont = Cliente::where('documento',$request->input('nit'))->where('id','!=',$request->id)->count();
            if ($cont == 0) {
                $cliente = Cliente::where('id',$request->id)->update([
                    'nombre' => $request->input('nombre'),
                    'documento' => $request->input('nit'),
                    'direccion' => $request->input('direccion'),                    
                    'correo' => $request->input('email')
                ]);
                $cliente = Cliente::where('id', $request->id)->first();
                return response()->json(['message' => "Cliente Editado Exitosamente", "data" => $cliente], 200); 
            }else{
                throw new \Exception("El cliente ya existe"); 
            }
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{                   
            $cliente = Cliente::find($request->id);
            Cliente::destroy($request->id);   
            return response()->json(['message' => "Cliente Eliminado Exitosamente", "data" => $cliente], 200);
        }catch (\Exception $e){
            return response()->json(['message' => 'Error No se puede eliminar el cliente ya que se encuentra asignado a una accion del sistema'], 404);
        }
    }
}
