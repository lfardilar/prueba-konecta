(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Administracion_Usuarios_InputUsuariosComponent_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue?vue&type=script&lang=js":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue?vue&type=script&lang=js ***!
  \***********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  props: {
    usuarios: {},
    usu: {},
    editarUsu: false
  },
  data: function data() {
    return {
      usuario: {
        nombre: "",
        email: "",
        password: "",
        passwordconf: "",
        role: 0,
        cliente: 0
      },
      clientes: [],
      roles: [],
      error: null,
      message: []
    };
  },
  mounted: function mounted() {
    this.getClients();
    this.getRoles();
  },
  methods: {
    forceRerender: function forceRerender(data) {
      this.usuarios = this.usuarios;
    },
    //Peticion para obtener los clientes a asociar
    getClients: function getClients() {
      var _this = this;

      axios.get("getClients").then(function (res) {
        _this.clientes = res.data;
      })["catch"](function (error) {
        console.log(error);
      });
    },
    //Peticion para obtener los roles a asociar
    getRoles: function getRoles() {
      var _this2 = this;

      axios.get("getRoles").then(function (res) {
        _this2.roles = res.data;
      })["catch"](function (error) {
        console.log(error);
      });
    },
    //Peticion para editar los usuarios
    editar: function editar() {
      var _this3 = this;

      if (this.usu.nombre.trim() === "" || this.usu.email.trim() === "" || this.usu.role === "0" || this.usu.cliente === "") {
        alert("Debes completar todos los campos requeridos");
        return;
      }

      var params = {
        name: this.usu.nombre,
        email: this.usu.email,
        roles: this.usu.role,
        cliente: this.usu.cliente
      };
      axios.post("users/".concat(this.usu.id, "/update"), params).then(function (res) {
        _this3.usu.nombre = "";
        _this3.usu.email = "";
        _this3.usu.role = "";
        _this3.usu.cliente = "";

        var index = _this3.usuarios.findIndex(function (usuarioBuscar) {
          return usuarioBuscar.id === res.data.id;
        });

        _this3.usuarios[index] = res.data;

        _this3.modUsu();

        _this3.$swal({
          icon: "success",
          title: "Usuario Editado Exitosamente"
        });
      })["catch"](function (error) {
        console.log(error);
        var errores = Object.values(error.response.data.errors);

        _this3.$swal({
          icon: "error",
          title: error.response.data.message,
          text: errores
        });

        _this3.error = error.response.data.errors;
        _this3.message = error.response.data.message;
      });
    },
    //Peticion para crear los usuarios
    crear: function crear() {
      var _this4 = this;

      if (this.usuario.nombre.trim() === "" || this.usuario.email.trim() === "" || this.usuario.password.trim() === "" || this.usuario.role === "0" || this.usuario.cliente === "0") {
        alert("Debes completar todos los campos requeridos");
        return;
      }

      var params = {
        name: this.usuario.nombre,
        email: this.usuario.email,
        password: this.usuario.password,
        password_confirmation: this.usuario.passwordconf,
        roles: this.usuario.role,
        cliente: this.usuario.cliente
      };
      axios.post("users/store", params).then(function (res) {
        _this4.usuario.nombre = "";
        _this4.usuario.email = "";
        _this4.usuario.password = "";
        _this4.usuario.passwordconf = "";
        _this4.usuario.role = "";
        _this4.usuario.cliente = "";

        _this4.usuarios.push(res.data);

        _this4.$swal({
          icon: "success",
          title: "Usuario Creado Exitosamente"
        });
      })["catch"](function (error) {
        console.log(error);
        var errores = Object.values(error.response.data.errors);

        _this4.$swal({
          icon: "error",
          title: error.response.data.message,
          text: errores
        });

        _this4.error = error.response.data.errors;
        _this4.message = error.response.data.message;
      });
    },
    //Validacion formulario crear o editar desde el componente padre
    accion: function accion() {
      this.$emit("accion");
    },
    //Modificar el usuario desde el componente padre
    modUsu: function modUsu() {
      this.$emit("modificarUsuario");
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue?vue&type=template&id=939285fe":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue?vue&type=template&id=939285fe ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render)
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

var _hoisted_1 = {
  "class": "box box-default p-3 mb-2 bg-white text-dark"
};
var _hoisted_2 = {
  "class": "box-body"
};

var _hoisted_3 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("span", {
  "class": "fa fa-plus fa-1x"
}, null, -1
/* HOISTED */
);

var _hoisted_4 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Nuevo Usuario ");

var _hoisted_5 = {
  "class": "modal fade",
  id: "modalCreateUser",
  tabindex: "-1",
  role: "dialog",
  "aria-labelledby": "modalCreateUser",
  "aria-hidden": "true"
};
var _hoisted_6 = {
  "class": "modal-dialog",
  role: "document"
};
var _hoisted_7 = {
  "class": "modal-content"
};
var _hoisted_8 = {
  "class": "modal-header"
};
var _hoisted_9 = {
  key: 0,
  "class": "modal-title"
};

var _hoisted_10 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("i", {
  "class": "fa fa-users"
}, null, -1
/* HOISTED */
);

var _hoisted_11 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)("Formulario de edición de usuarios ");

var _hoisted_12 = {
  key: 1,
  "class": "modal-title"
};

var _hoisted_13 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("i", {
  "class": "fa fa-users"
}, null, -1
/* HOISTED */
);

var _hoisted_14 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)("Formulario de creación de usuarios ");

var _hoisted_15 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("button", {
  type: "button",
  "class": "close",
  "data-dismiss": "modal",
  "aria-label": "Close"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("span", {
  "aria-hidden": "true"
}, "×")], -1
/* HOISTED */
);

var _hoisted_16 = {
  "class": "modal-body"
};
var _hoisted_17 = {
  "class": "form-group"
};

var _hoisted_18 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("label", {
  "for": "name",
  "class": "text-md-right"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)("Nombre"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("strong", {
  style: {
    "color": "red"
  }
}, "*")], -1
/* HOISTED */
);

var _hoisted_19 = {
  "class": "form-group"
};

var _hoisted_20 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("label", {
  "for": "email",
  "class": "text-md-right"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)("Email"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("strong", {
  style: {
    "color": "red"
  }
}, "*")], -1
/* HOISTED */
);

var _hoisted_21 = {
  "class": "form-group"
};

var _hoisted_22 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("label", {
  "for": "roles",
  "class": "text-center"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)("Nivel de usuario"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("strong", {
  style: {
    "color": "red"
  }
}, "*")], -1
/* HOISTED */
);

var _hoisted_23 = {
  "class": "form-group"
};

var _hoisted_24 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("label", {
  "for": "cliente",
  "class": "text-center"
}, "Asociación de cliente", -1
/* HOISTED */
);

var _hoisted_25 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("option", {
  value: 0,
  selected: true,
  disabled: true
}, "No Aplica", -1
/* HOISTED */
);

var _hoisted_26 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "modal-footer"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("button", {
  type: "submit",
  "class": "btn btn-sm btn-primary"
}, " Editar "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("button", {
  type: "button",
  "class": "btn btn-sm btn-danger",
  "data-dismiss": "modal"
}, " Cancelar ")], -1
/* HOISTED */
);

var _hoisted_27 = {
  "class": "form-group"
};

var _hoisted_28 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("label", {
  "for": "name",
  "class": "text-md-right"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)("Nombre"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("strong", {
  style: {
    "color": "red"
  }
}, "*")], -1
/* HOISTED */
);

var _hoisted_29 = {
  "class": "form-group"
};

var _hoisted_30 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("label", {
  "for": "email",
  "class": "text-md-right"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)("Email"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("strong", {
  style: {
    "color": "red"
  }
}, "*")], -1
/* HOISTED */
);

var _hoisted_31 = {
  "class": "form-group"
};

var _hoisted_32 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("label", {
  "for": "password",
  "class": "text-md-right"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)("Password"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("strong", {
  style: {
    "color": "red"
  }
}, "*")], -1
/* HOISTED */
);

var _hoisted_33 = {
  "class": "form-group"
};

var _hoisted_34 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("label", {
  "for": "password_confirmation",
  "class": "text-md-right"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)("Confirmar Password"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("strong", {
  style: {
    "color": "red"
  }
}, "*")], -1
/* HOISTED */
);

var _hoisted_35 = {
  "class": "form-group"
};

var _hoisted_36 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("label", {
  "for": "roles",
  "class": "text-center"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)("Nivel de usuario"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("strong", {
  style: {
    "color": "red"
  }
}, "*")], -1
/* HOISTED */
);

var _hoisted_37 = {
  "class": "form-group"
};

var _hoisted_38 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("label", {
  "for": "cliente",
  "class": "text-center"
}, "Asociación de cliente", -1
/* HOISTED */
);

var _hoisted_39 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("option", {
  value: 0,
  selected: true,
  disabled: true
}, "No Aplica", -1
/* HOISTED */
);

var _hoisted_40 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "modal-footer"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("button", {
  type: "submit",
  "class": "btn btn-sm btn-primary"
}, " Agregar "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("button", {
  type: "button",
  "class": "btn btn-sm btn-danger",
  "data-dismiss": "modal"
}, " Cancelar ")], -1
/* HOISTED */
);

function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("modal formulario usuario"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("a", {
    href: "#",
    "class": "btn btn-sm btn-primary",
    "data-toggle": "modal",
    "data-target": "#modalCreateUser",
    onClick: _cache[1] || (_cache[1] = function () {
      return $options.accion && $options.accion.apply($options, arguments);
    })
  }, [_hoisted_3, _hoisted_4])])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_6, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_7, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_8, [$props.editarUsu ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)("h5", _hoisted_9, [_hoisted_10, _hoisted_11])) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)("h5", _hoisted_12, [_hoisted_13, _hoisted_14])), _hoisted_15]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_16, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("formulario Editar"), $props.editarUsu ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)("form", {
    key: 0,
    onSubmit: _cache[6] || (_cache[6] = (0,vue__WEBPACK_IMPORTED_MODULE_0__.withModifiers)(function () {
      return $options.editar && $options.editar.apply($options, arguments);
    }, ["prevent"]))
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_17, [_hoisted_18, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("input", {
    type: "text",
    "class": "form-control",
    placeholder: "Nombre Completo",
    required: "",
    "onUpdate:modelValue": _cache[2] || (_cache[2] = function ($event) {
      return $props.usu.nombre = $event;
    })
  }, null, 512
  /* NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $props.usu.nombre]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_19, [_hoisted_20, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("input", {
    type: "email",
    "class": "form-control",
    placeholder: "Email",
    required: "",
    "onUpdate:modelValue": _cache[3] || (_cache[3] = function ($event) {
      return $props.usu.email = $event;
    })
  }, null, 512
  /* NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $props.usu.email]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_21, [_hoisted_22, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("select", {
    "class": "form-control",
    name: "roles",
    required: "",
    "onUpdate:modelValue": _cache[4] || (_cache[4] = function ($event) {
      return $props.usu.role = $event;
    })
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($data.roles, function (rol) {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)("option", {
      key: rol.id,
      value: rol.id
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(rol.name), 9
    /* TEXT, PROPS */
    , ["value"]);
  }), 128
  /* KEYED_FRAGMENT */
  ))], 512
  /* NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $props.usu.role]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_23, [_hoisted_24, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("select", {
    "class": "form-control select2",
    "onUpdate:modelValue": _cache[5] || (_cache[5] = function ($event) {
      return $props.usu.cliente = $event;
    }),
    placeholder: "Ingrese Cliente Asociado"
  }, [_hoisted_25, ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($data.clientes, function (cliente) {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)("option", {
      key: cliente.id,
      value: cliente.id
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(cliente.nombre), 9
    /* TEXT, PROPS */
    , ["value"]);
  }), 128
  /* KEYED_FRAGMENT */
  ))], 512
  /* NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $props.usu.cliente]])]), _hoisted_26], 32
  /* HYDRATE_EVENTS */
  )) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    key: 1
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("formulario Crear"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("form", {
    onSubmit: _cache[13] || (_cache[13] = (0,vue__WEBPACK_IMPORTED_MODULE_0__.withModifiers)(function () {
      return $options.crear && $options.crear.apply($options, arguments);
    }, ["prevent"]))
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_27, [_hoisted_28, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("input", {
    type: "text",
    "class": "form-control",
    placeholder: "Nombre Completo",
    required: "",
    "onUpdate:modelValue": _cache[7] || (_cache[7] = function ($event) {
      return $data.usuario.nombre = $event;
    })
  }, null, 512
  /* NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $data.usuario.nombre]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_29, [_hoisted_30, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("input", {
    type: "email",
    "class": "form-control",
    placeholder: "Email",
    required: "",
    "onUpdate:modelValue": _cache[8] || (_cache[8] = function ($event) {
      return $data.usuario.email = $event;
    })
  }, null, 512
  /* NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $data.usuario.email]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_31, [_hoisted_32, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("input", {
    type: "password",
    "class": "form-control",
    placeholder: "Password",
    required: "",
    "onUpdate:modelValue": _cache[9] || (_cache[9] = function ($event) {
      return $data.usuario.password = $event;
    })
  }, null, 512
  /* NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $data.usuario.password]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_33, [_hoisted_34, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("input", {
    type: "password",
    "class": "form-control",
    placeholder: "Confirmar Password",
    required: "",
    "onUpdate:modelValue": _cache[10] || (_cache[10] = function ($event) {
      return $data.usuario.passwordconf = $event;
    })
  }, null, 512
  /* NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $data.usuario.passwordconf]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_35, [_hoisted_36, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("select", {
    "class": "form-control",
    name: "roles",
    required: "",
    "onUpdate:modelValue": _cache[11] || (_cache[11] = function ($event) {
      return $data.usuario.role = $event;
    })
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($data.roles, function (rol) {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)("option", {
      key: rol.id,
      value: rol.id
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(rol.name), 9
    /* TEXT, PROPS */
    , ["value"]);
  }), 128
  /* KEYED_FRAGMENT */
  ))], 512
  /* NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $data.usuario.role]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_37, [_hoisted_38, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("select", {
    "class": "form-control select2",
    "onUpdate:modelValue": _cache[12] || (_cache[12] = function ($event) {
      return $data.usuario.cliente = $event;
    }),
    placeholder: "Ingrese Cliente Asociado"
  }, [_hoisted_39, ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($data.clientes, function (cliente) {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)("option", {
      key: cliente.id,
      value: cliente.id
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(cliente.nombre), 9
    /* TEXT, PROPS */
    , ["value"]);
  }), 128
  /* KEYED_FRAGMENT */
  ))], 512
  /* NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $data.usuario.cliente]])]), _hoisted_40], 32
  /* HYDRATE_EVENTS */
  )], 2112
  /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  ))])])])])])], 2112
  /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}

/***/ }),

/***/ "./resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue":
/*!*******************************************************************************!*\
  !*** ./resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _InputUsuariosComponent_vue_vue_type_template_id_939285fe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./InputUsuariosComponent.vue?vue&type=template&id=939285fe */ "./resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue?vue&type=template&id=939285fe");
/* harmony import */ var _InputUsuariosComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./InputUsuariosComponent.vue?vue&type=script&lang=js */ "./resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue?vue&type=script&lang=js");



_InputUsuariosComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__.default.render = _InputUsuariosComponent_vue_vue_type_template_id_939285fe__WEBPACK_IMPORTED_MODULE_0__.render
/* hot reload */
if (false) {}

_InputUsuariosComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__.default.__file = "resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue"

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_InputUsuariosComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__.default);

/***/ }),

/***/ "./resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue?vue&type=script&lang=js":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue?vue&type=script&lang=js ***!
  \*******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_InputUsuariosComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__.default)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_InputUsuariosComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./InputUsuariosComponent.vue?vue&type=script&lang=js */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue?vue&type=script&lang=js");
 

/***/ }),

/***/ "./resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue?vue&type=template&id=939285fe":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue?vue&type=template&id=939285fe ***!
  \*************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_InputUsuariosComponent_vue_vue_type_template_id_939285fe__WEBPACK_IMPORTED_MODULE_0__.render)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_InputUsuariosComponent_vue_vue_type_template_id_939285fe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!../../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./InputUsuariosComponent.vue?vue&type=template&id=939285fe */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Usuarios/InputUsuariosComponent.vue?vue&type=template&id=939285fe");


/***/ })

}]);