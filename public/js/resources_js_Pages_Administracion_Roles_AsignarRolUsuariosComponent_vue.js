(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Administracion_Roles_AsignarRolUsuariosComponent_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue?vue&type=script&lang=js":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue?vue&type=script&lang=js ***!
  \*************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _InputRolesComponent__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./InputRolesComponent */ "./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue");
//Importacion Componentes

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  data: function data() {
    return {
      preload: true,
      usuariosAsignados: [],
      usuariosPorAsignar: [],
      role: [],
      fields: [{
        key: "acciones",
        label: "Acciones"
      }, {
        key: "usuario",
        label: "Usuario",
        sortable: true,
        sortDirection: "desc"
      }, {
        key: "email",
        label: "Email",
        sortable: true,
        sortDirection: "desc"
      }],
      striped: true,
      bordered: true,
      borderless: false,
      outlined: true,
      small: false,
      hover: false,
      dark: false,
      fixed: true,
      footClone: false,
      headVariant: "ligth",
      tableVariant: "ligth",
      noCollapse: false,
      totalRows: 1,
      totalRowsAsi: 1,
      currentPage: 1,
      currentPageAsi: 1,
      perPage: 10,
      perPageAsi: 10,
      pageOptions: [10, 25, 50, 100],
      sortBy: "",
      sortDesc: false,
      sortDirection: "asc",
      filter: null,
      filterAsi: null,
      filterOn: []
    };
  },
  components: {},
  computed: {},
  mounted: function mounted() {
    this.getRole(this.$route.params.id);
    this.getUsuariosAsignados(this.$route.params.id);
  },
  methods: {
    onFilteredAsi: function onFilteredAsi(filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.totalRowsAsi = filteredItems.length;
      this.currentPageAsi = 1;
    },
    onFiltered: function onFiltered(filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.totalRows = filteredItems.length;
      this.currentPage = 1;
    },
    //Peticion para obtener el rol
    getRole: function getRole(role) {
      var _this = this;

      axios.get("".concat(role, "/usersRol")).then(function (res) {
        _this.role = res.data;
      })["catch"](function (error) {
        console.log(error);
      });
    },
    //Peticion para obtener los usuarios asignados al rol
    getUsuariosAsignados: function getUsuariosAsignados(role) {
      var _this2 = this;

      axios.get("".concat(role, "/ajaxPrintUsersRol")).then(function (res) {
        _this2.usuariosAsignados = res.data.usersHasRole;
        _this2.usuariosPorAsignar = res.data.users;
        _this2.totalRowsAsi = _this2.usuariosAsignados.length;
        _this2.totalRows = _this2.usuariosPorAsignar.length;
        _this2.preload = !_this2.preload;
      })["catch"](function (error) {
        console.log(error);
      });
    },
    //Peticion para remover el usuario del rol
    removerUsuario: function removerUsuario(item, index) {
      var _this3 = this;

      this.$swal({
        title: "Estas seguro?",
        text: "¡No podrás revertir esto!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "No, Cancelar",
        confirmButtonText: "Si, Remover"
      }).then(function (result) {
        if (result.isConfirmed) {
          var params = {
            idUser: item.id,
            stage: "remove"
          };
          axios.post("".concat(_this3.role.id, "/ajaxAdminUsersRol"), params).then(function (res) {
            var index = _this3.usuariosAsignados.findIndex(function (usuBuscar) {
              return usuBuscar.id == res.data.id;
            });

            _this3.usuariosAsignados.splice(index, 1);

            _this3.totalRows = _this3.usuariosAsignados.length;
            _this3.usuariosPorAsignar[index].isadd = 0;

            _this3.$swal("Removido", "Se removio el registro.", "success");
          })["catch"](function (error) {
            console.log(error);

            _this3.$swal({
              icon: "error",
              title: error.response.data.message,
              text: error.response.data.errors
            });
          });
        }
      });
    },
    //Peticion para agregar el usuario al rol
    agregarUsuario: function agregarUsuario(item, index) {
      var _this4 = this;

      this.$swal({
        title: "Estas seguro?",
        text: "¡No podrás revertir esto!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "No, Cancelar",
        confirmButtonText: "Si, Agregar"
      }).then(function (result) {
        if (result.isConfirmed) {
          var params = {
            idUser: item.id,
            stage: "add"
          };
          axios.post("".concat(_this4.role.id, "/ajaxAdminUsersRol"), params).then(function (res) {
            _this4.usuariosAsignados.push(res.data[0]);

            console.log(res.data[0].id);

            var index = _this4.usuariosPorAsignar.findIndex(function (usuarioBuscar) {
              return usuarioBuscar.id == res.data[0].id;
            });

            console.log(index);
            _this4.usuariosPorAsignar[index].isadd = 1;
            _this4.totalRows = _this4.usuariosAsignados.length;

            _this4.$swal("Agregado", "Se ha agregado el registro.", "success");
          })["catch"](function (error) {
            console.log(error);

            _this4.$swal({
              icon: "error",
              title: error.response.data.message,
              text: error.response.data.errors
            });
          });
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue?vue&type=script&lang=js":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue?vue&type=script&lang=js ***!
  \*****************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  props: {
    roles: {}
  },
  data: function data() {
    return {
      rol: {
        nombre: ""
      },
      error: null,
      message: []
    };
  },
  mounted: function mounted() {},
  methods: {
    //Peticion para crear el rol
    crear: function crear() {
      var _this = this;

      if (this.rol.nombre.trim() === "") {
        alert("Debes completar todos los campos requeridos");
        return;
      }

      var params = {
        role: this.rol.nombre
      };
      axios.post("store", params).then(function (res) {
        _this.rol.nombre = "";

        _this.roles.push(res.data);

        _this.$swal({
          icon: "success",
          title: "Rol Creado Exitosamente"
        });
      })["catch"](function (error) {
        console.log(error.response.data.message);
        var errores = Object.values(error.response.data.message);

        _this.$swal({
          icon: "error",
          title: "error",
          text: errores
        });

        _this.error = error.response;
        _this.message = error.response;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue?vue&type=template&id=48e18c6f":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue?vue&type=template&id=48e18c6f ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render)
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

var _hoisted_1 = {
  id: "page-wrapper"
};
var _hoisted_2 = {
  "class": "box box-default p-3 mb-2 bg-white text-dark"
};
var _hoisted_3 = {
  "class": "box-body"
};

var _hoisted_4 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("span", {
  "class": "fa fa-arrow-left"
}, null, -1
/* HOISTED */
);

var _hoisted_5 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Roles");

var _hoisted_6 = {
  "class": "box box-default p-3 mb-2 bg-white text-dark"
};
var _hoisted_7 = {
  "class": "box-body"
};

var _hoisted_8 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("h4", {
  style: {
    "background-color": "#f7f7f7",
    "font-size": "18px",
    "text-align": "center",
    "padding": "7px 10px",
    "margin-top": "0"
  }
}, " Asignacion de usuarios para el rol ", -1
/* HOISTED */
);

var _hoisted_9 = {
  "class": "media"
};
var _hoisted_10 = {
  "class": "media-body"
};
var _hoisted_11 = {
  "class": "clearfix"
};
var _hoisted_12 = {
  "class": "col-md-12"
};
var _hoisted_13 = {
  "class": "table"
};

var _hoisted_14 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("th", null, "Rol", -1
/* HOISTED */
);

var _hoisted_15 = {
  "class": "row"
};
var _hoisted_16 = {
  "class": "table-responsive col-md-6"
};

var _hoisted_17 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)("Limpiar");

var _hoisted_18 = {
  "class": "text-center text-info my-2"
};

var _hoisted_19 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("strong", null, "Cargando...", -1
/* HOISTED */
);

var _hoisted_20 = {
  "class": "table-responsive col-md-6"
};

var _hoisted_21 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)("Limpiar");

var _hoisted_22 = {
  "class": "text-center text-info my-2"
};

var _hoisted_23 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("strong", null, "Cargando...", -1
/* HOISTED */
);

function render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_router_link = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("router-link");

  var _component_b_form_select = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("b-form-select");

  var _component_b_form_group = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("b-form-group");

  var _component_b_col = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("b-col");

  var _component_b_form_input = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("b-form-input");

  var _component_b_button = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("b-button");

  var _component_b_input_group_append = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("b-input-group-append");

  var _component_b_input_group = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("b-input-group");

  var _component_b_row = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("b-row");

  var _component_b_spinner = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("b-spinner");

  var _component_b_link = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("b-link");

  var _component_b_table = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("b-table");

  var _component_b_pagination = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("b-pagination");

  var _component_b_container = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("b-container");

  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)("div", null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_router_link, {
    to: {
      name: 'VistaRoles'
    },
    "class": "btn btn-sm btn-primary"
  }, {
    "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
      return [_hoisted_4, _hoisted_5];
    }),
    _: 1
    /* STABLE */

  })])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_6, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_7, [_hoisted_8, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_9, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_10, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_11, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_12, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("table", _hoisted_13, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("tr", null, [_hoisted_14, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("td", null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($data.role.name), 1
  /* TEXT */
  )])])])])])])])])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_15, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_16, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_container, {
    fluid: ""
  }, {
    "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
      return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" interfas datatable "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_row, null, {
        "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
          return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_col, {
            sm: "5",
            md: "4",
            "class": "my-1"
          }, {
            "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
              return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_form_group, {
                label: "Mostrar",
                "label-for": "per-page-select",
                "label-cols-sm": "6",
                "label-cols-md": "4",
                "label-cols-lg": "4",
                "label-align-sm": "center",
                "label-size": "sm",
                "class": "mb-0"
              }, {
                "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
                  return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_form_select, {
                    id: "per-page-select",
                    modelValue: $data.perPageAsi,
                    "onUpdate:modelValue": _cache[1] || (_cache[1] = function ($event) {
                      return $data.perPageAsi = $event;
                    }),
                    options: $data.pageOptions,
                    size: "sm"
                  }, null, 8
                  /* PROPS */
                  , ["modelValue", "options"])];
                }),
                _: 1
                /* STABLE */

              })];
            }),
            _: 1
            /* STABLE */

          }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_col, {
            lg: "8",
            "class": "my-1"
          }, {
            "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
              return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_form_group, {
                label: "Buscar",
                "label-for": "filter-input",
                "label-cols-sm": "3",
                "label-align-sm": "right",
                "label-size": "sm",
                "class": "mb-0"
              }, {
                "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
                  return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_input_group, {
                    size: "sm"
                  }, {
                    "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
                      return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_form_input, {
                        id: "filter-input",
                        modelValue: $data.filterAsi,
                        "onUpdate:modelValue": _cache[2] || (_cache[2] = function ($event) {
                          return $data.filterAsi = $event;
                        }),
                        type: "search",
                        placeholder: "Buscar"
                      }, null, 8
                      /* PROPS */
                      , ["modelValue"]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_input_group_append, null, {
                        "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
                          return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_button, {
                            disabled: !$data.filterAsi,
                            onClick: _cache[3] || (_cache[3] = function ($event) {
                              return $data.filterAsi = '';
                            })
                          }, {
                            "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
                              return [_hoisted_17];
                            }),
                            _: 1
                            /* STABLE */

                          }, 8
                          /* PROPS */
                          , ["disabled"])];
                        }),
                        _: 1
                        /* STABLE */

                      })];
                    }),
                    _: 1
                    /* STABLE */

                  })];
                }),
                _: 1
                /* STABLE */

              })];
            }),
            _: 1
            /* STABLE */

          })];
        }),
        _: 1
        /* STABLE */

      }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" elementos datatable "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_table, {
        busy: $data.preload,
        striped: $data.striped,
        bordered: $data.bordered,
        borderless: $data.borderless,
        outlined: $data.outlined,
        small: $data.small,
        hover: $data.hover,
        dark: $data.dark,
        fixed: $data.fixed,
        "foot-clone": $data.footClone,
        "no-border-collapse": $data.noCollapse,
        items: $data.usuariosAsignados,
        fields: $data.fields,
        "head-variant": $data.headVariant,
        "table-variant": $data.tableVariant,
        "current-page": $data.currentPageAsi,
        "per-page": $data.perPageAsi,
        filter: $data.filterAsi,
        "filter-included-fields": $data.filterOn,
        "sort-by": $data.sortBy,
        "sort-desc": $data.sortDesc,
        "sort-direction": $data.sortDirection,
        stacked: "sm",
        "show-empty": "",
        onFiltered: $options.onFilteredAsi
      }, {
        "table-busy": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
          return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_18, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_spinner, {
            "class": "align-middle"
          }), _hoisted_19])];
        }),
        "cell(name)": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function (row) {
          return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)((0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(row.value.first) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(row.value.last), 1
          /* TEXT */
          )];
        }),
        "cell(acciones)": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function (row) {
          return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_link, {
            href: "#",
            "data-toggle": "tooltip",
            title: "Remover usuario"
          }, {
            "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
              return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("i", {
                "class": "fa fa-fw fa-toggle-on",
                title: "acciones",
                onClick: function onClick($event) {
                  return $options.removerUsuario(row.item, row.index);
                }
              }, null, 8
              /* PROPS */
              , ["onClick"])];
            }),
            _: 2
            /* DYNAMIC */

          }, 1024
          /* DYNAMIC_SLOTS */
          )])];
        }),
        _: 1
        /* STABLE */

      }, 8
      /* PROPS */
      , ["busy", "striped", "bordered", "borderless", "outlined", "small", "hover", "dark", "fixed", "foot-clone", "no-border-collapse", "items", "fields", "head-variant", "table-variant", "current-page", "per-page", "filter", "filter-included-fields", "sort-by", "sort-desc", "sort-direction", "onFiltered"]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("Paginador"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_col, {
        sm: "7",
        md: "16",
        "class": "my-1"
      }, {
        "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
          return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_pagination, {
            modelValue: $data.currentPageAsi,
            "onUpdate:modelValue": _cache[4] || (_cache[4] = function ($event) {
              return $data.currentPageAsi = $event;
            }),
            "total-rows": $data.totalRowsAsi,
            "per-page": $data.perPageAsi,
            align: "fill",
            size: "sm",
            "class": "my-0"
          }, null, 8
          /* PROPS */
          , ["modelValue", "total-rows", "per-page"])];
        }),
        _: 1
        /* STABLE */

      })];
    }),
    _: 1
    /* STABLE */

  })]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_20, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_container, {
    fluid: ""
  }, {
    "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
      return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" interfas datatable "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_row, null, {
        "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
          return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_col, {
            sm: "5",
            md: "4",
            "class": "my-1"
          }, {
            "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
              return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_form_group, {
                label: "Mostrar",
                "label-for": "per-page-select",
                "label-cols-sm": "6",
                "label-cols-md": "4",
                "label-cols-lg": "4",
                "label-align-sm": "center",
                "label-size": "sm",
                "class": "mb-0"
              }, {
                "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
                  return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_form_select, {
                    id: "per-page-select",
                    modelValue: $data.perPage,
                    "onUpdate:modelValue": _cache[5] || (_cache[5] = function ($event) {
                      return $data.perPage = $event;
                    }),
                    options: $data.pageOptions,
                    size: "sm"
                  }, null, 8
                  /* PROPS */
                  , ["modelValue", "options"])];
                }),
                _: 1
                /* STABLE */

              })];
            }),
            _: 1
            /* STABLE */

          }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_col, {
            lg: "8",
            "class": "my-1"
          }, {
            "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
              return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_form_group, {
                label: "Buscar",
                "label-for": "filter-input",
                "label-cols-sm": "3",
                "label-align-sm": "right",
                "label-size": "sm",
                "class": "mb-0"
              }, {
                "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
                  return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_input_group, {
                    size: "sm"
                  }, {
                    "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
                      return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_form_input, {
                        id: "filter-input",
                        modelValue: $data.filter,
                        "onUpdate:modelValue": _cache[6] || (_cache[6] = function ($event) {
                          return $data.filter = $event;
                        }),
                        type: "search",
                        placeholder: "Buscar"
                      }, null, 8
                      /* PROPS */
                      , ["modelValue"]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_input_group_append, null, {
                        "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
                          return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_button, {
                            disabled: !$data.filter,
                            onClick: _cache[7] || (_cache[7] = function ($event) {
                              return $data.filter = '';
                            })
                          }, {
                            "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
                              return [_hoisted_21];
                            }),
                            _: 1
                            /* STABLE */

                          }, 8
                          /* PROPS */
                          , ["disabled"])];
                        }),
                        _: 1
                        /* STABLE */

                      })];
                    }),
                    _: 1
                    /* STABLE */

                  })];
                }),
                _: 1
                /* STABLE */

              })];
            }),
            _: 1
            /* STABLE */

          })];
        }),
        _: 1
        /* STABLE */

      }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" elementos datatable "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_table, {
        busy: $data.preload,
        striped: $data.striped,
        bordered: $data.bordered,
        borderless: $data.borderless,
        outlined: $data.outlined,
        small: $data.small,
        hover: $data.hover,
        dark: $data.dark,
        fixed: $data.fixed,
        "foot-clone": $data.footClone,
        "no-border-collapse": $data.noCollapse,
        items: $data.usuariosPorAsignar,
        fields: $data.fields,
        "head-variant": $data.headVariant,
        "table-variant": $data.tableVariant,
        "current-page": $data.currentPage,
        "per-page": $data.perPage,
        filter: $data.filter,
        "filter-included-fields": $data.filterOn,
        "sort-by": $data.sortBy,
        "sort-desc": $data.sortDesc,
        "sort-direction": $data.sortDirection,
        stacked: "sm",
        "show-empty": "",
        onFiltered: $options.onFiltered
      }, {
        "table-busy": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
          return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_22, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_spinner, {
            "class": "align-middle"
          }), _hoisted_23])];
        }),
        "cell(name)": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function (row) {
          return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)((0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(row.value.first) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(row.value.last), 1
          /* TEXT */
          )];
        }),
        "cell(acciones)": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function (row) {
          return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", null, [row.item.isadd == 1 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_b_link, {
            key: 0,
            "data-toggle": "tooltip",
            title: "Remover usuario"
          }, {
            "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
              return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("i", {
                "class": "fa fa-fw fa-toggle-on",
                title: "acciones",
                onClick: function onClick($event) {
                  return $options.removerUsuario(row.item, row.index);
                }
              }, null, 8
              /* PROPS */
              , ["onClick"])];
            }),
            _: 2
            /* DYNAMIC */

          }, 1024
          /* DYNAMIC_SLOTS */
          )) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_b_link, {
            key: 1,
            "data-toggle": "tooltip",
            title: "Agregar usuario"
          }, {
            "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
              return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("i", {
                "class": "fa fa-fw fa-toggle-off",
                title: "acciones",
                onClick: function onClick($event) {
                  return $options.agregarUsuario(row.item, row.index);
                }
              }, null, 8
              /* PROPS */
              , ["onClick"])];
            }),
            _: 2
            /* DYNAMIC */

          }, 1024
          /* DYNAMIC_SLOTS */
          ))])];
        }),
        _: 1
        /* STABLE */

      }, 8
      /* PROPS */
      , ["busy", "striped", "bordered", "borderless", "outlined", "small", "hover", "dark", "fixed", "foot-clone", "no-border-collapse", "items", "fields", "head-variant", "table-variant", "current-page", "per-page", "filter", "filter-included-fields", "sort-by", "sort-desc", "sort-direction", "onFiltered"]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("Paginador"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_col, {
        sm: "7",
        md: "16",
        "class": "my-1"
      }, {
        "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
          return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_b_pagination, {
            modelValue: $data.currentPage,
            "onUpdate:modelValue": _cache[8] || (_cache[8] = function ($event) {
              return $data.currentPage = $event;
            }),
            "total-rows": $data.totalRows,
            "per-page": $data.perPage,
            align: "fill",
            size: "sm",
            "class": "my-0"
          }, null, 8
          /* PROPS */
          , ["modelValue", "total-rows", "per-page"])];
        }),
        _: 1
        /* STABLE */

      })];
    }),
    _: 1
    /* STABLE */

  })])])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue?vue&type=template&id=d7a08d7e":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue?vue&type=template&id=d7a08d7e ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render)
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");


var _hoisted_1 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "box box-default p-3 mb-2 bg-white text-dark"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "box-body"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("a", {
  href: "#",
  "class": "btn btn-sm btn-primary",
  "data-toggle": "modal",
  "data-target": "#modalCreateRol"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("span", {
  "class": "fa fa-plus fa-1x"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Nuevo Rol ")])])], -1
/* HOISTED */
);

var _hoisted_2 = {
  "class": "modal fade",
  id: "modalCreateRol",
  tabindex: "-1",
  role: "dialog",
  "aria-labelledby": "modalCreateRol",
  "aria-hidden": "true"
};
var _hoisted_3 = {
  "class": "modal-dialog",
  role: "document"
};
var _hoisted_4 = {
  "class": "modal-content"
};

var _hoisted_5 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createStaticVNode)("<div class=\"modal-header\"><h5 class=\"modal-title\"><i class=\"fa fa-users\"></i>Formulario de creación de roles </h5><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button></div>", 1);

var _hoisted_6 = {
  "class": "modal-body"
};
var _hoisted_7 = {
  "class": "form-group"
};

var _hoisted_8 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("label", {
  "for": "name",
  "class": "text-md-right"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)("Rol"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("strong", {
  style: {
    "color": "red"
  }
}, "*")], -1
/* HOISTED */
);

var _hoisted_9 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "modal-footer"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("button", {
  type: "submit",
  "class": "btn btn-sm btn-primary"
}, " Agregar "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("button", {
  type: "button",
  "class": "btn btn-sm btn-danger",
  "data-dismiss": "modal"
}, " Cancelar ")], -1
/* HOISTED */
);

function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("modal formulario usuario"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", null, [_hoisted_1, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_4, [_hoisted_5, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_6, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("formulario Crear"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("form", {
    onSubmit: _cache[2] || (_cache[2] = (0,vue__WEBPACK_IMPORTED_MODULE_0__.withModifiers)(function () {
      return $options.crear && $options.crear.apply($options, arguments);
    }, ["prevent"]))
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_7, [_hoisted_8, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("input", {
    type: "text",
    "class": "form-control",
    placeholder: "Nombre",
    required: "",
    "onUpdate:modelValue": _cache[1] || (_cache[1] = function ($event) {
      return $data.rol.nombre = $event;
    })
  }, null, 512
  /* NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $data.rol.nombre]])]), _hoisted_9], 32
  /* HYDRATE_EVENTS */
  )])])])])])], 2112
  /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}

/***/ }),

/***/ "./resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue":
/*!*********************************************************************************!*\
  !*** ./resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _AsignarRolUsuariosComponent_vue_vue_type_template_id_48e18c6f__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AsignarRolUsuariosComponent.vue?vue&type=template&id=48e18c6f */ "./resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue?vue&type=template&id=48e18c6f");
/* harmony import */ var _AsignarRolUsuariosComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AsignarRolUsuariosComponent.vue?vue&type=script&lang=js */ "./resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue?vue&type=script&lang=js");



_AsignarRolUsuariosComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__.default.render = _AsignarRolUsuariosComponent_vue_vue_type_template_id_48e18c6f__WEBPACK_IMPORTED_MODULE_0__.render
/* hot reload */
if (false) {}

_AsignarRolUsuariosComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__.default.__file = "resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue"

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_AsignarRolUsuariosComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__.default);

/***/ }),

/***/ "./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue":
/*!*************************************************************************!*\
  !*** ./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _InputRolesComponent_vue_vue_type_template_id_d7a08d7e__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./InputRolesComponent.vue?vue&type=template&id=d7a08d7e */ "./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue?vue&type=template&id=d7a08d7e");
/* harmony import */ var _InputRolesComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./InputRolesComponent.vue?vue&type=script&lang=js */ "./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue?vue&type=script&lang=js");



_InputRolesComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__.default.render = _InputRolesComponent_vue_vue_type_template_id_d7a08d7e__WEBPACK_IMPORTED_MODULE_0__.render
/* hot reload */
if (false) {}

_InputRolesComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__.default.__file = "resources/js/Pages/Administracion/Roles/InputRolesComponent.vue"

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_InputRolesComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__.default);

/***/ }),

/***/ "./resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue?vue&type=script&lang=js":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue?vue&type=script&lang=js ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_AsignarRolUsuariosComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__.default)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_AsignarRolUsuariosComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./AsignarRolUsuariosComponent.vue?vue&type=script&lang=js */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue?vue&type=script&lang=js");
 

/***/ }),

/***/ "./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue?vue&type=script&lang=js":
/*!*************************************************************************************************!*\
  !*** ./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue?vue&type=script&lang=js ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_InputRolesComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__.default)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_InputRolesComponent_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./InputRolesComponent.vue?vue&type=script&lang=js */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue?vue&type=script&lang=js");
 

/***/ }),

/***/ "./resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue?vue&type=template&id=48e18c6f":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue?vue&type=template&id=48e18c6f ***!
  \***************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_AsignarRolUsuariosComponent_vue_vue_type_template_id_48e18c6f__WEBPACK_IMPORTED_MODULE_0__.render)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_AsignarRolUsuariosComponent_vue_vue_type_template_id_48e18c6f__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!../../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./AsignarRolUsuariosComponent.vue?vue&type=template&id=48e18c6f */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Roles/AsignarRolUsuariosComponent.vue?vue&type=template&id=48e18c6f");


/***/ }),

/***/ "./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue?vue&type=template&id=d7a08d7e":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue?vue&type=template&id=d7a08d7e ***!
  \*******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_InputRolesComponent_vue_vue_type_template_id_d7a08d7e__WEBPACK_IMPORTED_MODULE_0__.render)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_InputRolesComponent_vue_vue_type_template_id_d7a08d7e__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!../../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./InputRolesComponent.vue?vue&type=template&id=d7a08d7e */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Administracion/Roles/InputRolesComponent.vue?vue&type=template&id=d7a08d7e");


/***/ })

}]);