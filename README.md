# Prueba Konecta

Prueba tecnica Konecta

# Requisitos
Laravel >= 8
Vuejs = 3.0 
PHP >= 7.3
Composer >= 2.0
Nodejs >= 14.16.1

# Desplegue 
* Clonar el repositorio de gitlab con el comando * git clone https://gitlab.com/lfardilar/prueba-konecta.git
* Ingresamos a la carpeta generada prueba-konecta e instalamos
    * Los paquetes de laravel con el comando * composer install 
    * Los paquetes de JS con el comando * npm i
* Ingresamos a la carpeta generada prueba-konecta y configuramos
    * Ingresamos al archivo .env y configuramos nuestras  variables de conexion a la base de datos
        DB_CONNECTION="Driver conexion"
        DB_HOST="Ip de servidor base de datos"
        DB_PORT="Puerto de servidor base de datos"
        DB_DATABASE="nombre basededatos de servidor base de datos"
        DB_USERNAME="nombre usuario de servidor base de datos"
        DB_PASSWORD="Pass de servidor base de datos"
    * Generamos la llave de seguridad con el comando * php artisan key:generate
* Ingresamos a la carpeta generada prueba-konecta y ejecutamos
    * Los migrate y seeder con los comandos
    * php artisan migrate --seed    
    * El complilador de JS con el comando * npm run dev
    * La llave secreta de JWT con * php artisan jwt:secret
* Iniciamos el proyecto ya sea desde un servidor apache o con el comando * php artisan serve 
* Ingresamos con usuario administrador Administrador@konecta.com password '*123456'
el usuario administrador puede realizar todas las acciones del sistema

*Se debe crear rol vendedor y asignarle los permisos index_clientes, acciones_clientes
*Una vez creado el rol se creara un nuevo usuario y se asignara el rol vendedor el cual solo podra realizar gestionar los clientes.